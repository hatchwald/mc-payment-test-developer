<?php
$num = [3, 1, 2, 4];
$result = [];
for ($i = 0; $i < count($num); $i++) {
    $temp = [];
    for ($x = 0; $x < count($num); $x++) {
        if ($num[$i] - $num[$x] < 0) {
            $temp[] = $num[$i];
        }
    }

    if (count($temp) == 0) {
        $result[] = $num[$i];
    }
}

echo json_encode($result);
