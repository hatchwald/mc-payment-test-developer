<?php
$num = [1, 2, 3, 4];
$x = 4;
$result = [];
for ($i = 0; $i < count($num); $i++) {
    $temp = [];
    for ($y = 0; $y < count($num); $y++) {
        if ($num[$i] / $num[$y] == $x) {
            continue;
        } else {
            $temp[] = $num[$i];
        }
    }

    if (count($temp) == count($num)) {
        $result[] = $num[$i];
    }
}

echo json_encode($result);
